<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pantalla o vista inicial donde se puede iniciar sesion o dar click en registrarse
Route::get('/', 'AuthController@index');

//Ruta POST para realizar la autenticacion del usuario que desea iniciar sesion
Route::post('/', 'AuthController@login');

//Ruta para cargar la vista del registro de usuarios 
Route::get('/registrarse','UsersController@registrarse');

//Ruta POST para realizar el registro/agregar usuarios
Route::post('/registrarse', 'UsersController@add');

//Ruta para cargar la vista de editar un perfil (candidato, empresa)
Route::post('/edit_perfil', 'UsersController@edit');

Route::get('/edit_perfil', 'UsersController@editar');

//Ruta para cargar la vista de agregar ofertas laborales, pantalla a lo que solo empresas pueden acceder muy obviamente
Route::get('add_oferta', 'OfertasLaboralesController@index');

//Ruta para realizar el proceso de cierre de sesion y volver a la pantalla de inicio
Route::get('/logout', 'UsersController@logout');

//Ruta para realizar el metodo de agregar un requisito laboral
Route::post('/add_oferta', 'OfertasLaboralesController@agregar_requisito');

Route::get('/principalC','PrincipalCandidatoController@index');

Route::get('/aplicar/{id}','PrincipalCandidatoController@aplicar');

Route::get('/noAplicar/{id}','PrincipalCandidatoController@noAplicar');

Route::get('eliminarOferta/{id}','PrincipalEmpresaController@eliminarOferta');

// Ruta o direccion para la vista agregar/editar un curriculum de un candidato
Route::get('/add_curriculum', 'CurriculumsController@index');

// Ruta o direccion para la vista donde las empresas agregaran/editaran ofertas laborales
Route::get('/agregar_oferta', function () {
    return view('empresas.ofertas_laborales.add_oferta');
});
Route::get('/principalE','PrincipalEmpresaController@index');

// Ruta para ingresar a la vista de agregar una experiencia profesional a un curriculum
Route::get('/experiencias','ExperienciasProfesionalesController@index');

Route::post('/experiencias','ExperienciasProfesionalesController@add');

Route::get('/formaciones','formacionesAcademicasController@index');

Route::post('/formaciones','formacionesAcademicasController@add');

Route::get('/candidatos/{id}','candidatosController@index');

Route::post('/add_curriculum','CurriculumsController@add');

Route::get('/eliminarFormacion/{id}','CurriculumsController@eliminarFormacion');

Route::get('/eliminarExperiencias/{id}','CurriculumsController@eliminarExperiencias');

Route::get('/eliminarUsuario','UsersController@delete');

Route::get('/editarUsuario','UsersController@edit');

Route::get('/home', 'HomeController@index')->name('home');





