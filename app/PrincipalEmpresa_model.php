<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrincipalEmpresa_model extends Model
{
    public function get_categorias(){
        $categorias = DB::table('tbl_categorias_laborales')->get();
        return $categorias;
    }

    public function get_ofertas($user){
        $e = DB::table('tbl_empresas')->select('id_empresa')->where('id_usuario',$user)->limit(1)->get();
        $ofertas = DB::table('tbl_ofertas_laborales')->where('id_empresa',$e[0]->id_empresa)->get();
        foreach($ofertas as $o){
            $co = DB::table('tbl_categorias_laborales')->select('categoria')->where('id_categoria_laboral',$o->id_categoria_laboral)->limit(1)->get();
            $o->categoria=$co[0]->categoria;    
        }
        return $ofertas;
    }
    public function delete_ofertas($oferta){
        DB::table('tbl_ofertas_laborales')->where('id_oferta_laboral',$oferta)->delete();
    }
}
