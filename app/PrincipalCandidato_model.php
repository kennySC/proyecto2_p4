<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrincipalCandidato_model extends Model
{
    public function get_categorias(){
        $categorias = DB::table('tbl_categorias_laborales')->get();
        return $categorias;
    }
    public function get_ofertas($user){
        $curriculum =DB::table('tbl_curriculums')->select('id_curriculum')->leftJoin('tbl_candidatos', 'tbl_candidatos.id_candidato', '=', 'tbl_curriculums.id_candidato')->where('tbl_candidatos.id_usuario',$user)->limit(1)->get();
        $ofertas = DB::table('tbl_ofertas_laborales')->get();
       foreach($ofertas as $o){
            $co = DB::table('tbl_categorias_laborales')->select('categoria')->where('id_categoria_laboral',$o->id_categoria_laboral)->limit(1)->get();
            $o->categoria=$co[0]->categoria;
            $e = DB::table('tbl_empresas')->select('id_usuario')->where('id_empresa',$o->id_empresa)->limit(1)->get();
            $u = DB::table('tbl_usuarios')->select('nombre_real')->where('id_usuario',$e[0]->id_usuario)->limit(1)->get();
            $a = DB::table('tbl_curriculums_ofertas')->where([['id_curriculum','=', $curriculum[0]->id_curriculum],['id_oferta_laboral','=',$o->id_oferta_laboral],])->count();
             if($a==0){
                $o->aplicado = false;
             }else{
                $o->aplicado = true;
             }
            $o->empresa=$u[0]->nombre_real;  
        }
        return $ofertas;
    }
    public function addAplicaion($user,$oferta){
        $curriculum =DB::table('tbl_curriculums')->select('id_curriculum')->leftJoin('tbl_candidatos', 'tbl_candidatos.id_candidato', '=', 'tbl_curriculums.id_candidato')->where('tbl_candidatos.id_usuario',$user)->limit(1)->get();
        
        DB::table('tbl_curriculums_ofertas')->insert([
            ['id_curriculum' => $curriculum[0]->id_curriculum, 'id_oferta_laboral' => $oferta]
        ]);
    }

    public function deleteAplicaion($user,$oferta){
        $curriculum =DB::table('tbl_curriculums')->select('id_curriculum')->leftJoin('tbl_candidatos', 'tbl_candidatos.id_candidato', '=', 'tbl_curriculums.id_candidato')->where('tbl_candidatos.id_usuario',$user)->limit(1)->get();
        DB::table('tbl_curriculums_ofertas')->where([['id_curriculum','=', $curriculum[0]->id_curriculum],['id_oferta_laboral','=',$oferta],])->delete();
    }
}
