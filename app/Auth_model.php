<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use DB;

    class Auth_model extends Model{

        public function login($data){
            $usuario_existe = $this->validar_usuario($data['username']); 
            
            if($usuario_existe != false && Hash::check($data['password'], $usuario_existe->password)){
                return true; //El usuario existe, y fue autenticado
            }
            else{
                return false; //NO autenticado
            }
        }

        public function validar_usuario($username){
            $query = DB::table('tbl_usuarios')->select('username', 'password', 'tipo_usuario')->where('username', $username)->count(); // Nos devuelve solo una fila, no se necesita comprobar
            if($query == 1){
                $usuario = DB::table('tbl_usuarios')->select('*')->where('username', $username)->first();
                return $usuario;
            }
            else{
                return false;
            }
        }
        
    }