<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Curriculum_model extends Model
{

    public function get_curriculum($user){
        
        
        $curriculum =DB::table('tbl_curriculums')->leftJoin('tbl_candidatos', 'tbl_candidatos.id_candidato', '=', 'tbl_curriculums.id_candidato')->where('tbl_candidatos.id_usuario',$user)->limit(1)->get();
        return $curriculum;

    }
    public function get_experiencias($curriculum){
      
        $experiencias = DB::table('tbl_experiencias_profesionales')->leftJoin('tbl_curriculum_experiencias', 'tbl_curriculum_experiencias.id_experiencias_profesionales', '=', 'tbl_experiencias_profesionales.id_experiencias_profesionales')->where('tbl_curriculum_experiencias.id_curriculum',$curriculum)->get();
        return $experiencias;
    }
    public function get_formaciones($curriculum){
        $formaciones = DB::table('tbl_formaciones_academicas')->leftJoin('tbl_curriculum_formaciones', 'tbl_curriculum_formaciones.id_formaciones_academicas', '=', 'tbl_formaciones_academicas.id_formaciones_academicas')->where('tbl_curriculum_formaciones.id_curriculum',$curriculum)->get();
        return $formaciones;
    }
    public function update_curriculum($datos, $curriculum){
        DB::table('tbl_curriculums')
              ->where('id_curriculum', $curriculum)
              ->update(['meritos_observaciones' => $datos]);
    }
    public function delete_formaciones($id){
        DB::table('tbl_formaciones_academicas')->where('id_formaciones_academicas',$id)->delete();
    }
    public function delete_experiencias($id){
        DB::table('tbl_experiencias_profesionales')->where('id_experiencias_profesionales',$id)->delete();
    }
}
