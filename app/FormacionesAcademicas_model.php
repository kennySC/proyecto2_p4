<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class FormacionesAcademicas_model extends Model
{
    public function addFormacion($data,$curriculum){
        $formacion = DB::table('tbl_formaciones_academicas')->insertGetId(
            [ 'titulo' =>$data['titulo'],
            'especialidad' => $data['especialidad'], 
            'institucion' => $data['institucion'],
            'fecha' => $data['fecha']]
        );
        DB::table('tbl_curriculum_formaciones')->insert(
            ['id_curriculum'=>$curriculum,
            'id_formaciones_academicas' => $formacion]
        );
    }

   /* public function get_formaciones(){
        $formaciones = DB::table('tbl_formaciones_academicas')->get();
        return $formaciones;
    }*/
}
