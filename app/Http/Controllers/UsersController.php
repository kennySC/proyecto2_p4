<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User_model;
use Alert;

class UsersController extends Controller
{

    public function __construct()
    {
        session_start();
        $this->User_model = new User_model();
    }

    public function registrarse()
    {
        return view('users.add');
    }

    function add(Request $req)
    {
        if (isset($_POST['rb_tipo_usuario'])) { //Primero tenemos que consultar si esta seteado el radio button
            $tipo_user = $_POST['rb_tipo_usuario'];
        }
        $validacion_datos = $req->validate([
            'txt_cedula' => 'required|max:15',
            'txt_username' => 'required|max:64',
            'txt_password' => 'required|max:64',
            'txt_nombre' => 'required|max:64',
            'txt_direccion' => 'required|max:264',
            'txt_telefono' => 'required|max:32',
            'txt_correo' => 'required|max:64',
        ]);

        $params = array(
            'cedula' => $req->input('txt_cedula'),
            'username' => $req->input('txt_username'),
            'password' => password_hash($req->input('txt_password'), PASSWORD_BCRYPT),
            'nombre_real' => $req->input('txt_nombre'),
            'direccion' => $req->input('txt_direccion'),
            'foto' => 'sin_foto',
            'telefono' => $req->input('txt_telefono'),
            'email' => $req->input('txt_correo'),
            'tipo_usuario' => $tipo_user,
        );
        $id_user = $this->User_model->agregar_usuario($params);

        $this->User_model->agregar_tipo_usuario($params['tipo_usuario'], $id_user); //Metodo que va a guardar en la tabla de Candidatos o Empresas el usuario
        Alert::success('Exito', 'Datos guardados con exito');
        return view('users.add'); // carga nuevamente la misma pagina

    }

    function edit(Request $request)
    {
        // If para validar que el usuario que va a editar sea el usuario due;o/logeado, o sea que no sea otro usuario editando el perfil de otro
        /*if(){
            
        }*/
        $params = array(
            'cedula' => $request->input('txt_cedula'),
            'username' => $request->input('txt_username'),
            'password' => password_hash($request->input('txt_password'), PASSWORD_BCRYPT),
            'nombre_real' => $request->input('txt_nombre'),
            'direccion' => $request->input('txt_direccion'),
            'foto' => 'sin_foto',
            'telefono' => $request->input('txt_telefono'),
            'email' => $request->input('txt_correo'),
        );
        
        // Este metodo nos retorna un true o un false, true = si edito, false = nel
        $editar_usuario = $this->User_model->editar_usuario($_SESSION['datos_usuario']['id_usuario'],$params); 
        if($editar_usuario>0){
            Alert::success('Exito', 'Datos guardados con exito.');
        }
        else if($editar_usuario==0){
            Alert::success('Error', 'Tus datos no pudieron guardarse, intenta de nuevo.');
        }
        return view('users.edit');
    }

    function delete(){
        $this->User_model->eliminar_usuario($_SESSION['datos_usuario']['id_usuario']);
        return redirect()->action('UsersController@logout');
    }

    function logout(Request $request)
    {
        //$request->session()->flush();
        unset($_SESSION['datos_usuario']);
        session_destroy();
        Alert::success('Has cerrado sesión con éxito.', 'Vuelve pronto!');
        return redirect()->action('AuthController@index');
    }
    function editar(){
        return view('users.edit');
    }
}
