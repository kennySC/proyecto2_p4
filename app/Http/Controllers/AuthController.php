<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Auth_model;
Use Alert;

class AuthController extends Controller{

    public function __construct(){
        $this->Auth_model = new Auth_model();
    }

    public function index(){
        return view('inicio');
    }

    public function login(Request $request){
        $data = array(
            'username' => $request->input('txt_username'),
            'password' => $request->input('txt_password')
        );

        $result_login = $this->Auth_model->login($data); // Metodo que nos devuelve un true o false de si existe o no el usuario
        
        if($result_login == TRUE){
            $username = $request->input('txt_username');
            $result_session = $this->Auth_model->validar_usuario($username);
            if($result_session!=false){
                $session_data = array(
                    'logged_in' => TRUE,
                    'id_usuario' => $result_session->id_usuario,
                    'cedula' => $result_session->cedula,
                    'username' => $result_session->username, 
                    'nombre_real' => $result_session->nombre_real,
                    'direccion' => $result_session->direccion,
                    'foto' => $result_session->foto,
                    'telefono' => $result_session->telefono,
                    'email'=> $result_session->email,
                    'tipo_usuario' => $result_session->tipo_usuario,
                );
                //$request->session()->put('datos_usuario', $session_data);
                session_start();
                $_SESSION['datos_usuario'] = $session_data;
                //$datos_session = $request->session()->all();

                if($result_session->tipo_usuario=='C'){
                    return redirect()->action('PrincipalCandidatoController@index');
                }
                else if($result_session->tipo_usuario=='E'){
                    return redirect()->action('PrincipalEmpresaController@index');  
                }
            }
        }
        else{
            //$this->index(); Se deberia de llamar a este metodo pero no lo agarra, es decir no entra a la vista
            Alert::error('Error al ingresar', 'Por favor revise los datos ingresados');
            return view('inicio'); // Si es la linea asi sola si entra
        }
    }


}