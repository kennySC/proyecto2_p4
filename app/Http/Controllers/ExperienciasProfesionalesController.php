<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExperienciasProfesionales_model;
use Alert;

class ExperienciasProfesionalesController extends Controller
{
    public function __construct()
    {
        $this->ExperienciasProfesionales_model = new ExperienciasProfesionales_model();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('experienciasProfesionales');
    }
    public function add(Request $request)
    {
        session_start();
        $validar_datos = $request->validate([
            'txt_puesto' => 'required|max:128',
            'txt_empresa' => 'required|max:128',
            'txt_descripcion' => 'required|max:532',
            'fecha_ini' => 'required',
            'fecha_fin' => 'required',
        ]);

        $data = array(
            'puesto' => $request->input('txt_puesto'),
            'empresa' => $request->input('txt_empresa'),
            'descrip_responsabilidades' => $request->input('txt_descripcion'),
            'fecha_inicio' => $request->input('fecha_ini'),
            'fecha_fin' => $request->input('fecha_fin')
        );

        $this->ExperienciasProfesionales_model->addExperienca($data, $_SESSION['curriculum']);
        Alert::success('Exito', 'Se han guardado los datos correctamente');
        return redirect()->action('ExperienciasProfesionalesController@index');
    }
}
