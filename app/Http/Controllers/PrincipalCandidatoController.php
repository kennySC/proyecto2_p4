<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrincipalCandidato_model;
Use Alert;

class PrincipalCandidatoController extends Controller
{
    public function __construct(){
        $this->PrincipalCandidato_model = new PrincipalCandidato_model();
    }
    
    public function index(){
        session_start();
        $data = array();
        $data['categorias'] = $this->PrincipalCandidato_model->get_categorias();
        $data['ofertas'] = $this->PrincipalCandidato_model->get_ofertas($_SESSION['datos_usuario']['id_usuario']);
        $data['datos_usuario']=$_SESSION['datos_usuario'];
        
        return view('principalCandidato')->with($data);
        
    }
    public function aplicar($oferta){
        session_start();
        $this->PrincipalCandidato_model->addAplicaion($_SESSION['datos_usuario']['id_usuario'],$oferta);
        return redirect()->action('PrincipalCandidatoController@index');
    }
    public function noAplicar($oferta){
        session_start();
        $this->PrincipalCandidato_model->deleteAplicaion($_SESSION['datos_usuario']['id_usuario'],$oferta);
        return redirect()->action('PrincipalCandidatoController@index');
    }
}
