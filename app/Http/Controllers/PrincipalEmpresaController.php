<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrincipalEmpresa_model;
Use Alert;

class PrincipalEmpresaController extends Controller
{
    public function __construct(){
        $this->PrincipalEmpresa_model = new PrincipalEmpresa_model();
    }

    public function index(){
        session_start();
        $data = array();
        $data['categorias'] = $this->PrincipalEmpresa_model->get_categorias();
        $data['ofertas'] = $this->PrincipalEmpresa_model->get_ofertas($_SESSION['datos_usuario']['id_usuario']);
        $data['datos_usuario']=$_SESSION['datos_usuario'];
        return view('PrincipalEmpresa')->with($data);
    }
    public function eliminarOferta($id){
       
        
       $this->PrincipalEmpresa_model->delete_ofertas($id);
       return redirect()->action('PrincipalEmpresaController@index');
    }
}
