<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\candidatos_model;

class candidatosController extends Controller
{
    public function __construct(){
        $this->candidatos_model = new candidatos_model();
    }

    public function index($id){
        
        $data['candidato'] = $this->candidatos_model->get_candidatos($id);
        return view('candidatos')->with($data);
    }
}
