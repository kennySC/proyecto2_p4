<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OfertasLaborales_model;
use App\RequisitosController;

    class OfertasLaboralesController extends Controller{

        public function __construct(){
            $this->OfertasLaborales_model = new OfertasLaborales_model();
            
        }

        public function index(){
            session_start();
            $categorias = $this->OfertasLaborales_model->get_categorias();
            $requisitos = $this->OfertasLaborales_model->get_requisitos();
            $datos = array();
            $data['categorias'] = $categorias;
            $data['requisitos'] = $requisitos;
           

            return view('empresas.ofertas_laborales.add_oferta')->with($data);
        }

        public function add(Request $request){ // Metodo para agregar ofertas laborales
            $validar_datos = $request->validate([
                'cant_vacantes' => 'required|min:1|max:11',
                'txt_fecha' => 'required|',
                'cmb_categoria' => 'required',
                'txt_ubicacion' => 'required|max:512',
                'txt_descripcion' => 'required|max:512',
            ]);

            if(!$validar_datos->fails()){
                $data = array(
                    'descrip_puesto' => $request->input('txt_descripcion'),
                    'fecha_oferta' => $request->input('txt_fecha'),
                    'ubicacion' => $request->input('txt_ubicacion'),
                    'id_categoria_laboral' => $request->input('cmb_categoria'),
                    'id_empresa' => '1',
                );
            }
           
        }

        public function agregar_requisito(Request $req){
            $validar = $req->validate([
                'txt_requisito' => 'required|max:256'
            ]);
            
            $requisito = $this->OfertasLaborales_model->add_requisito($req->input('txt_requisito')); 
            //$requisito = variable que es el ID del requisito que acabamos de ingresar para luego enviarlos cuando se inserte la oferta laboral
            return redirect()->action('OfertasLaboralesController@index');
        }

        public function editar_requisito(Request $req, $id_requsito){
            $validar = $req->validate([
                'txt_requisito' => 'required|max:256'
            ]);
            $requisito_editado = $this->OfertasLaborales_model->edit_requisito($id_requsito, $req->input('txt_requisito')); 
        }

        public function eliminar_requisito($id_requsito){
            $this->OfertasLaborales_model->delete_requisito($id_requsito); 
        }
    }