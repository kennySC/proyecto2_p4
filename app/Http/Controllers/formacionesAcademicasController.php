<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FormacionesAcademicas_model;
Use Alert;

class FormacionesAcademicasController extends Controller
{
    public function __construct(){
        $this->FormacionesAcademicas_model = new FormacionesAcademicas_model();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formacionesAcademicas');
    }

    public function add(Request $request)
    {   
        session_start();
        $validar_datos = $request->validate([
            'txt_titulo' => 'required|max:128',
            'txt_especialidad' => 'required|max:128',
            'txt_institucion' => 'required|max:128',
            'txt_fecha' => 'required',

        ]);
        
            $data = array(
                
                'titulo' =>$request->input('txt_titulo'),
                'especialidad' => $request->input('txt_especialidad'), 
                'institucion' => $request->input('txt_institucion'),
                'fecha' => $request->input('txt_fecha')
                
            );
            $this->FormacionesAcademicas_model->addFormacion($data,$_SESSION['curriculum']);
            Alert::success('Exito', 'Datos guardados con exito');
            return view('formacionesAcademicas');
        
        
    }
}
