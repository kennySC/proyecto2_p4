<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\ExperienciasProfesionales_model;
use App\FormacionesAcademicas_model;
use Illuminate\Http\Request;
use App\Curriculum_model;
Use Alert;

    class CurriculumsController extends Controller{
        
        public function __construct(){
            $this->Curriculum_model = new Curriculum_model();
            //$this->ExperienciasProfesionales_model = new ExperienciasProfesionales_model();
            //$this->FormacionesAcademicas_model = new FormacionesAcademicas_model();
        }

        public function index(){
            session_start();
            $data = array();
            $data = $this->obtener_datos();
            return view('curriculums.curriculum')->with($data);
        }

        public function obtener_datos(){ // Metodo para obtener las experiencias profesionales y formaciones academicas agregadas
            $experiencias= array();
            $formaciones = array();
            $curriculum = $this->Curriculum_model->get_curriculum($_SESSION['datos_usuario']['id_usuario']);
            $_SESSION['curriculum'] = $curriculum[0]->id_curriculum;
            $experiencias = $this->Curriculum_model->get_experiencias($curriculum[0]->id_curriculum);
            $formaciones = $this->Curriculum_model->get_formaciones($curriculum[0]->id_curriculum);
            $datos = array();
            $datos['curriculum']= $curriculum[0];
            $datos['experiencias'] = $experiencias;
            $datos['formaciones'] = $formaciones;
            return $datos;
        }
        public function add(Request $request){
            session_start();
            $data =$request->input('txt_observaciones');
            $this->Curriculum_model->update_curriculum($data,$_SESSION['curriculum']);
            Alert::success('Exito', 'Se han guardado los datos correctamente');
            return redirect()->action('CurriculumsController@index');
        }
        public function eliminarFormacion($id){
            $this->Curriculum_model->delete_formaciones($id);
            return redirect()->action('CurriculumsController@index');
        }
        public function eliminarExperiencias($id){
            $this->Curriculum_model->delete_experiencias($id);
            return redirect()->action('CurriculumsController@index');
        }
        
    }