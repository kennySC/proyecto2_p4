<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class candidatos_model extends Model
{
    public function get_candidatos($oferta){
        $candidatos = DB::table('tbl_usuarios')->join('tbl_candidatos','tbl_usuarios.id_usuario','=','tbl_candidatos.id_usuario')->join('tbl_curriculums','tbl_candidatos.id_candidato','=','tbl_curriculums.id_candidato')->join('tbl_curriculums_ofertas','tbl_curriculums.id_curriculum','=','tbl_curriculums_ofertas.id_curriculum')->where('tbl_curriculums_ofertas.id_oferta_laboral',$oferta)->get();
        return $candidatos;
    }
}
