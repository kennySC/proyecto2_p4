<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use DB;

    class OfertasLaborales_model extends Model{

        public function add_oferta(){

        }

        public function add_requisito($requisito){
            $requisito = DB::table('tbl_requisitos_laborales')->insertGetId([
                'requisito' => $requisito,
            ]);
            return $requisito; //retornamos el ID del requisito que acabamos de ingresar
        }

        public function get_requisitos(){
            $requisitos = DB::table('tbl_requisitos_laborales')->get();
            return $requisitos;
        }

        public function edit_requisito($id_requisito, $requisito){
            $requisito_actualizado = DB::table('tbl_requisitos_laborales')->where('id_requisito_laboral', '=', $id_requisito)->update($requisito);
            return $requisito_actualizado; //retornamos el id del requisito que acabamos de actualizar
        }

        public function delete_requisito($id_requisito){
            DB::table('tbl_requisitos_laborales')->where('id_requisito_laboral', '=', $id_requisito)->delete();
        }

        public function get_categorias(){
            $categorias = DB::table('tbl_categorias_laborales')->get();
            return $categorias;
        }
    }