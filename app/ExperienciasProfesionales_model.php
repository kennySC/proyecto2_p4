<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ExperienciasProfesionales_model extends Model
{
    public function addExperienca($data,$curriculum){
        $experiencia = DB::table('tbl_experiencias_profesionales')->insertGetId(
            [ 'puesto' =>$data['puesto'],
            'empresa' => $data['empresa'], 
            'descrip_responsabilidades' => $data['descrip_responsabilidades'],
            'fecha_inicio' => $data['fecha_inicio'], 
            'fecha_fin' =>$data['fecha_fin']]
        );
         
         DB::table('tbl_curriculum_experiencias')->insert(
             ['id_curriculum'=>$curriculum,
             'id_experiencias_profesionales' => $experiencia]
         );
        
    }

   /* public function get_experiencias(){
        $experiencias = DB::table('tbl_experiencias_profesionales')->get();
        return $experiencias;
    }*/
}
