<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

    class User_model extends Model{
        function __construct(){
            parent::__construct();
        }

        function obtener_usuario($id_usuario){
            return $this->db->query("SELECT tbl_usuarios.* FROM tbl_usuarios WHERE tbl_usuarios.id_usuario = " . $id_usuario)->row_array();
        }

        function agregar_usuario($params){
            $ss = DB::table('tbl_usuarios')->insertGetId(
                [
                    'cedula' => $params['cedula'],
                    'username' => $params['username'],
                    'password' => $params['password'],
                    'nombre_real' => $params['nombre_real'],
                    'direccion' => $params['direccion'],
                    'foto' => $params['foto'],
                    'telefono' => $params['telefono'],
                    'email' => $params['email'],
                    'tipo_usuario' => $params['tipo_usuario']
                ]
                );
            return $ss; // Retorna el id del usuario que acabamos de ingresar
        }

        // Metodo para agregar el usuario a la tabla que le corresponde digamos si es un candidato a la tabla candidatos si es empresa a la tabla empresa
        function agregar_tipo_usuario($tipo_user, $id_user){ 
            if($tipo_user == 'E'){
                $empresa = DB::table('tbl_empresas')->insertGetId(['id_usuario' => $id_user]);
                return $empresa;
            }
            else if($tipo_user == 'C'){
                $candidato = DB::table('tbl_candidatos')->insertGetId(['id_usuario'=> $id_user]);
                
               DB::table('tbl_curriculums')->insert(['id_candidato'=> $candidato]);
                return $candidato;
            }
        }

        function editar_usuario($id_usuario, $params){
            //El metodo update de laravel devuelve la cantidad de columnas editadas
            $user_editado = DB::table('tbl_usuarios')->where('id_usuario',$id_usuario)->update([
                    'cedula' => $params['cedula'],
                    'username' => $params['username'],
                    'password' => $params['password'],
                    'nombre_real' => $params['nombre_real'],
                    'direccion' => $params['direccion'],
                    'telefono' => $params['telefono'],
                    'email' => $params['email'],
            ]);
            // Entonces si nos devuelve un numero mayor a cero significa que si edito (ya sea 1 o N campos)
            if($user_editado>0){ 
                return true;
            }
            // Si retorno cero significa que no edito nada, nos sirve para validar y ver si SI se efectuo el query
            else if($user_editado==0){
                return false;
            }
        }

        function eliminar_usuario($id_usuario){
            DB::table('tbl_usuarios')->where('id_usuario', $id_usuario)->delete();
        }
    }