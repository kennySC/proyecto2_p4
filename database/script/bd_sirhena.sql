-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-06-2020 a las 00:05:49
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_sirhena`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_candidatos`
--

CREATE TABLE `tbl_candidatos` (
  `id_candidato` bigint(3) UNSIGNED NOT NULL,
  `id_usuario` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_candidatos`
--

INSERT INTO `tbl_candidatos` (`id_candidato`, `id_usuario`) VALUES
(3, 24),
(4, 27);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_categorias_laborales`
--

CREATE TABLE `tbl_categorias_laborales` (
  `id_categoria_laboral` bigint(3) UNSIGNED NOT NULL,
  `categoria` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_categorias_laborales`
--

INSERT INTO `tbl_categorias_laborales` (`id_categoria_laboral`, `categoria`) VALUES
(1, 'Informatica'),
(2, 'Administrativa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_curriculums`
--

CREATE TABLE `tbl_curriculums` (
  `id_curriculum` bigint(3) UNSIGNED NOT NULL,
  `meritos_observaciones` varchar(256) DEFAULT NULL,
  `id_candidato` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_curriculums_ofertas`
--

CREATE TABLE `tbl_curriculums_ofertas` (
  `id_curriculum` bigint(3) UNSIGNED NOT NULL,
  `id_oferta_laboral` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla intermedia entre los curriculums y ofertas laborales';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_curriculum_experiencias`
--

CREATE TABLE `tbl_curriculum_experiencias` (
  `id_curriculum` bigint(3) UNSIGNED NOT NULL,
  `id_experiencias_profesionales` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla intermedia entre los curriculums y las experiencias profesionales';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_curriculum_formaciones`
--

CREATE TABLE `tbl_curriculum_formaciones` (
  `id_curriculum` bigint(3) UNSIGNED NOT NULL,
  `id_formaciones_academicas` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla intermedia entre los curriculums y las formaciones academicas';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_empresas`
--

CREATE TABLE `tbl_empresas` (
  `id_empresa` bigint(3) UNSIGNED NOT NULL,
  `id_usuario` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_empresas`
--

INSERT INTO `tbl_empresas` (`id_empresa`, `id_usuario`) VALUES
(1, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_experiencias_profesionales`
--

CREATE TABLE `tbl_experiencias_profesionales` (
  `id_experiencias_profesionales` bigint(3) UNSIGNED NOT NULL,
  `puesto` varchar(128) NOT NULL,
  `empresa` varchar(128) NOT NULL,
  `descrip_responsabilidades` varchar(532) NOT NULL,
  `fecha_inicio` varchar(20) NOT NULL,
  `fecha_fin` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_experiencias_profesionales`
--

INSERT INTO `tbl_experiencias_profesionales` (`id_experiencias_profesionales`, `puesto`, `empresa`, `descrip_responsabilidades`, `fecha_inicio`, `fecha_fin`) VALUES
(1, 'Taxista', 'Porteadores del Sur', 'Realizar viajes en la zona sur', '2020-04-07', '2020-03-10'),
(2, 'ADC', 'Fnatic', 'Carrear la bot lane', '2019-12-03', '2020-01-25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_formaciones_academicas`
--

CREATE TABLE `tbl_formaciones_academicas` (
  `id_formaciones_academicas` bigint(3) UNSIGNED NOT NULL,
  `titulo` varchar(128) NOT NULL,
  `especialidad` varchar(128) NOT NULL,
  `institucion` varchar(128) NOT NULL,
  `fecha` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ofertas_laborales`
--

CREATE TABLE `tbl_ofertas_laborales` (
  `id_oferta_laboral` bigint(3) UNSIGNED NOT NULL,
  `descrip_puesto` varchar(512) NOT NULL,
  `cantidad_vacantes` int(11) NOT NULL,
  `fecha_oferta` varchar(20) NOT NULL,
  `ubicacion` varchar(512) NOT NULL,
  `id_categoria_laboral` bigint(3) UNSIGNED NOT NULL,
  `id_empresa` bigint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ofertas_requisitos`
--

CREATE TABLE `tbl_ofertas_requisitos` (
  `id_oferta_laboral` bigint(3) UNSIGNED NOT NULL,
  `id_requisito_laboral` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla intermedia entre las ofertas laborales y sus requisitos';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_requisitos_laborales`
--

CREATE TABLE `tbl_requisitos_laborales` (
  `id_requisito_laboral` bigint(3) UNSIGNED NOT NULL,
  `requisito` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_requisitos_laborales`
--

INSERT INTO `tbl_requisitos_laborales` (`id_requisito_laboral`, `requisito`) VALUES
(3, 'Trabajar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `id_usuario` bigint(3) UNSIGNED NOT NULL,
  `cedula` varchar(15) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `nombre_real` varchar(64) NOT NULL,
  `direccion` varchar(264) NOT NULL,
  `foto` varchar(128) DEFAULT NULL,
  `telefono` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `tipo_usuario` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`id_usuario`, `cedula`, `username`, `password`, `nombre_real`, `direccion`, `foto`, `telefono`, `email`, `tipo_usuario`) VALUES
(14, '117010462', 'pato', '$2y$10$DzfJFIVJEABsqQiPwWwB4OhUvAJIecMPCKoSAZM0fbBiYXpvon1UC', 'Matthew', 'PZ', 'sin_foto', '89987656', 'elpato10miranda@gmail.com', 'E'),
(24, '12341234', 'qwerqwe', '$2y$10$3HfMElgLvQROr/BFugTZ3OZNgADfUYKzAUSdly2RVuTNDQQQdu4OC', 'qwerqwer', 'qwerwqer', 'sin_foto', '1341234', 'qsdfa@gmail.com', 'C'),
(25, '12341234', 'wewqe', '$2y$10$n4SGIKZZJgoJqRQRNZujUObGD/aIANAZse8bYTWrwkHKvtp8StbXK', 'werqwe', 'wqerqw', 'sin_foto', 'e1324123', 'qwer@gmail.com', 'E'),
(26, '12341234', 'wwert', '$2y$10$4iMVYTZfOo2OlqIzCIXUKuS9uqCtZ6UOfDABNOlBKZtWQMaF/kTw.', 'wertwer', 'ertwert', 'sin_foto', '2435234', 'dfgs@gmaill.com', 'E'),
(27, '2345234', '1', '$2y$10$93z8WVxSwG38xvKyLdWnEOE/m7apqGLTaelSw1vLjpW9ME1cduXSi', 'qwerwer', 'wqerwqer', 'sin_foto', '1234623456', '1@gmail.com', 'C');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_candidatos`
--
ALTER TABLE `tbl_candidatos`
  ADD PRIMARY KEY (`id_candidato`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `tbl_categorias_laborales`
--
ALTER TABLE `tbl_categorias_laborales`
  ADD PRIMARY KEY (`id_categoria_laboral`);

--
-- Indices de la tabla `tbl_curriculums`
--
ALTER TABLE `tbl_curriculums`
  ADD PRIMARY KEY (`id_curriculum`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `tbl_curriculums_ofertas`
--
ALTER TABLE `tbl_curriculums_ofertas`
  ADD KEY `id_curriculum` (`id_curriculum`),
  ADD KEY `id_oferta_laboral` (`id_oferta_laboral`);

--
-- Indices de la tabla `tbl_curriculum_experiencias`
--
ALTER TABLE `tbl_curriculum_experiencias`
  ADD KEY `id_curriculum` (`id_curriculum`),
  ADD KEY `id_experiencias_profesionales` (`id_experiencias_profesionales`);

--
-- Indices de la tabla `tbl_curriculum_formaciones`
--
ALTER TABLE `tbl_curriculum_formaciones`
  ADD KEY `id_curriculum` (`id_curriculum`),
  ADD KEY `id_formaciones_academicas` (`id_formaciones_academicas`);

--
-- Indices de la tabla `tbl_empresas`
--
ALTER TABLE `tbl_empresas`
  ADD PRIMARY KEY (`id_empresa`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `tbl_experiencias_profesionales`
--
ALTER TABLE `tbl_experiencias_profesionales`
  ADD PRIMARY KEY (`id_experiencias_profesionales`);

--
-- Indices de la tabla `tbl_formaciones_academicas`
--
ALTER TABLE `tbl_formaciones_academicas`
  ADD PRIMARY KEY (`id_formaciones_academicas`);

--
-- Indices de la tabla `tbl_ofertas_laborales`
--
ALTER TABLE `tbl_ofertas_laborales`
  ADD PRIMARY KEY (`id_oferta_laboral`),
  ADD KEY `id_categoria_laboral` (`id_categoria_laboral`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `tbl_ofertas_requisitos`
--
ALTER TABLE `tbl_ofertas_requisitos`
  ADD KEY `id_oferta_laboral` (`id_oferta_laboral`),
  ADD KEY `id_requisito_laboral` (`id_requisito_laboral`);

--
-- Indices de la tabla `tbl_requisitos_laborales`
--
ALTER TABLE `tbl_requisitos_laborales`
  ADD PRIMARY KEY (`id_requisito_laboral`);

--
-- Indices de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_candidatos`
--
ALTER TABLE `tbl_candidatos`
  MODIFY `id_candidato` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_categorias_laborales`
--
ALTER TABLE `tbl_categorias_laborales`
  MODIFY `id_categoria_laboral` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_curriculums`
--
ALTER TABLE `tbl_curriculums`
  MODIFY `id_curriculum` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_empresas`
--
ALTER TABLE `tbl_empresas`
  MODIFY `id_empresa` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_experiencias_profesionales`
--
ALTER TABLE `tbl_experiencias_profesionales`
  MODIFY `id_experiencias_profesionales` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_formaciones_academicas`
--
ALTER TABLE `tbl_formaciones_academicas`
  MODIFY `id_formaciones_academicas` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_ofertas_laborales`
--
ALTER TABLE `tbl_ofertas_laborales`
  MODIFY `id_oferta_laboral` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_requisitos_laborales`
--
ALTER TABLE `tbl_requisitos_laborales`
  MODIFY `id_requisito_laboral` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `id_usuario` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_candidatos`
--
ALTER TABLE `tbl_candidatos`
  ADD CONSTRAINT `tbl_candidatos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_curriculums`
--
ALTER TABLE `tbl_curriculums`
  ADD CONSTRAINT `tbl_curriculums_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `tbl_candidatos` (`id_candidato`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_curriculums_ofertas`
--
ALTER TABLE `tbl_curriculums_ofertas`
  ADD CONSTRAINT `tbl_curriculums_ofertas_ibfk_1` FOREIGN KEY (`id_curriculum`) REFERENCES `tbl_curriculums` (`id_curriculum`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_curriculums_ofertas_ibfk_2` FOREIGN KEY (`id_oferta_laboral`) REFERENCES `tbl_ofertas_laborales` (`id_oferta_laboral`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_curriculum_experiencias`
--
ALTER TABLE `tbl_curriculum_experiencias`
  ADD CONSTRAINT `tbl_curriculum_experiencias_ibfk_1` FOREIGN KEY (`id_curriculum`) REFERENCES `tbl_curriculums` (`id_curriculum`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_curriculum_experiencias_ibfk_2` FOREIGN KEY (`id_experiencias_profesionales`) REFERENCES `tbl_experiencias_profesionales` (`id_experiencias_profesionales`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_curriculum_formaciones`
--
ALTER TABLE `tbl_curriculum_formaciones`
  ADD CONSTRAINT `tbl_curriculum_formaciones_ibfk_1` FOREIGN KEY (`id_curriculum`) REFERENCES `tbl_curriculums` (`id_curriculum`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_curriculum_formaciones_ibfk_2` FOREIGN KEY (`id_formaciones_academicas`) REFERENCES `tbl_formaciones_academicas` (`id_formaciones_academicas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_empresas`
--
ALTER TABLE `tbl_empresas`
  ADD CONSTRAINT `tbl_empresas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_ofertas_laborales`
--
ALTER TABLE `tbl_ofertas_laborales`
  ADD CONSTRAINT `tbl_ofertas_laborales_ibfk_2` FOREIGN KEY (`id_categoria_laboral`) REFERENCES `tbl_categorias_laborales` (`id_categoria_laboral`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_ofertas_laborales_ibfk_3` FOREIGN KEY (`id_empresa`) REFERENCES `tbl_empresas` (`id_empresa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbl_ofertas_requisitos`
--
ALTER TABLE `tbl_ofertas_requisitos`
  ADD CONSTRAINT `tbl_ofertas_requisitos_ibfk_1` FOREIGN KEY (`id_oferta_laboral`) REFERENCES `tbl_ofertas_laborales` (`id_oferta_laboral`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_ofertas_requisitos_ibfk_2` FOREIGN KEY (`id_requisito_laboral`) REFERENCES `tbl_requisitos_laborales` (`id_requisito_laboral`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
