

    <!DOCTYPE html>
    <html>

    <head>
        <meta lang='en'>
        <style>
            html,
            body {
                background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
                /*color: #636b6f;*/
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: auto;
                overflow: auto;
            }

            #banner_box {
                position: relative;
                margin: auto auto;
                text-align: center;
                width: 100%;
                height: 108px;
                top: 0px;
                font-size: 20px;
                margin-bottom: 2.5%;
            }

            #banner {
                position: relative;
                object-fit: cover;
                object-position: center;
                height: 72px;
                width: 240px;

            }

            #formacion_box {
                top: 2.5%;
                position: relative;
                width: 90%;
                border-radius: 15px;
                border: 2px solid #33134C;
                height: 65%;
                display: block;
                margin-left: auto;
                margin-right: auto;
                background-color: white;
            }

            #btn_regresar {
                position: absolute;
                right: 5px;
                cursor: pointer;
            }

            #grid_txts {
                position: relative;
                top: 25%;
                display: inline-grid;
                grid-template-columns: auto auto;
                justify-content: space-evenly;
                grid-row-gap: 20px;
                width: 100%;
            }

            .cajatexto {
                position: relative;
                padding: 12.5px;
                margin: 10px;
                border-radius: 7px;
                border: 1px solid #660066;
                width: 250px;
                color: black;
                background: white;
                font-family: 'Nunito', sans-serif;
            }

            label {
                color: #33134C;
                font-size: 20px;
            }

            #btn_return {
                position: absolute;
                border-radius: 6px;
                top: 6.5px;
                margin-left: 96.5%;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                height: 30px;
                width: 30px;
            }

            #btn_return:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .btn_actions {
                font-family: 'Nunito', sans-serif;
                margin-top: 2.5%;
                font-size: 12px;
                position: relative;
                border-radius: 45px;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                font-size: 17px;
                font-weight: bold;
                position: relative;
                border: 0px;
                height: 60px;
                width: 35%;
            }

            .btn_actions:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .div_guardar {
                position: relative;
                text-align: right;
            }

            #btn_guardar {
                position: absolute;
                left: 75%;
            }
        </style>
    </head>

    <body>
        @include('sweetalert::alert')
        <div id="formacion_box">
            <div id='banner_box'>
                <img id="banner" src={{asset('img/SIRHENA_LOGO.png')}}>
                <h2>Formacion Academica</h2>
            </div>
            <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
            <form method="post">
                @csrf
                <br><div id="grid_txts">
                    <div>
                        <Label>Fecha de Titulacion: </Label>
                        <input type="date" name="txt_fecha" id="txt_fecha" class="cajatexto" placeholder="Fecha" required>
                    </div>
                    <input type="text" name="txt_especialidad" id="txt_especialidad" class="cajatexto" placeholder="Especialidad" maxlength="128" required>
                    <input type="text" name="txt_titulo" id="txt_titulo" class="cajatexto" placeholder="Titulo" maxlength="128" required>
                    <input type="text" name="txt_institucion" id="txt_institucion" class="cajatexto" placeholder="Institucion" maxlength="128" required>
                    <div class="div_guardar">
                        <input type="submit" value="GUARDAR" name="btn_guardar" id="btn_guardar" title="Guardar Formacion" class="btn_actions" /><br />
                    </div>
                </div>

        </div>
        </div>
    </body>

    </html>