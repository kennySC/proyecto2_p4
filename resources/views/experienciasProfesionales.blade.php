
    <!DOCTYPE html>
    <html>

    <head>
        <meta lang='en'>
        <style>
            html,
            body {
                background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
                /*color: #636b6f;*/
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: auto;
                overflow: auto;
            }

            #banner_box {
                position: absolute;
                margin: auto auto;
                text-align: center;
                width: 100%;
                height: 108px;
                top: 0px;
                font-size: 20px;
            }

            #banner {
                position: relative;
                object-fit: cover;
                object-position: center;
                height: 72px;
                width: 240px;

            }

            #experiencia_box {
                top: 2.5%;
                position: relative;
                width: 90%;
                border-radius: 15px;
                border: 2px solid #33134C;
                height: 500px;
                display: block;
                margin-left: auto;
                margin-right: auto;
                background-color: white;
            }

            #btn_regresar {
                position: absolute;
                right: 5px;
                cursor: pointer;
            }

            #grid_txts {
                position: absolute;
                top: 150px;
                display: inline-grid;
                grid-template-columns: auto auto;
                justify-content: space-evenly;
                grid-row-gap: 50px;
                width: 100%;
            }

            label {
                color: #33134C;
                font-size: 20px;
            }

            #btn_return {
                position: absolute;
                border-radius: 6px;
                top: 6.5px;
                margin-left: 96.5%;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                height: 30px;
                width: 30px;
            }

            #btn_return:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .btn_actions {
                font-family: 'Nunito', sans-serif;
                margin-top: 2.5%;
                font-size: 12px;
                position: relative;
                border-radius: 45px;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                font-size: 17px;
                font-weight: bold;
                position: relative;
                border: 0px;
                height: 60px;
                width: 35%;
            }

            .btn_actions:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .cajatexto {
                padding: 12.5px;
                margin: 10px;
                border-radius: 7px;
                border: 1px solid #660066;
                width: 250px;
                color: black;
                background: white;
                font-family: 'Nunito', sans-serif;
            }
            .div_txts{
                position: relative;
                text-align: left;
            }
            #txt_descripcion{
                position: relative;
                width: 90%;
            }
            .div_guardar{
                position: relative;
                text-align: right;
            }
            #btn_guardar{
                position: absolute;
                left: 75%;
            }
        </style>
    </head>

    <body>
        @include('sweetalert::alert')
        <div id="experiencia_box">
            <div id='banner_box'>
                <img id="banner" src={{asset('img/SIRHENA_LOGO.png')}}>
                <h2>Experiencia Profesional</h2>
            </div>
            <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
            
            <form method="post">
            <br>
                @csrf
                <div id="grid_txts">
                <div>
                        <Label>Fecha de inicio:</Label>
                        <input type="date" name="fecha_ini" id="fecha_ini" class="cajatexto" placeholder="Fecha de Inicio" required>
                    </div>
                    <div>
                        <Label>Fecha de Finalizacion:</Label>
                        <input type="date" name="fecha_fin" id="fecha_fin" class="cajatexto" placeholder="Fecha de Finalizacion" required>
                    </div>
                    <div id="div_txts">
                        <input type="text" name="txt_puesto" id="txt_puesto" class="cajatexto" placeholder="Puesto" maxlength="128" required>
                        <input type="text" name="txt_empresa" id="txt_empresa" class="cajatexto" placeholder="Empresa" maxlength="128" required>
                    </div>
                    <textarea name="txt_descripcion" id="txt_descripcion" class="cajatexto" placeholder="Descripcion de Responsabilidades" maxlength="532" required></textarea>
                    <div class="div_guardar">
                        <input type="submit" value="GUARDAR" name="btn_guardar" id="btn_guardar" title="Guardar Experiencia" class="btn_actions" /><br />
                    </div>
                </div>
        </div>
        </form>
    </body>

    </html>
