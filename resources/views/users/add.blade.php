<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SIRHENA-REGISTRARSE</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            html, body {
                background: radial-gradient(circle, rgba(166,14,146,0.9402135854341737) 1%, rgba(174,37,230,0.9009978991596639) 69%, rgba(132,20,214,0.9150035014005602) 96%);
                background-color: silver;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow: hidden;
            }

            .content{
                width: 100%;
            }

            .titulo{
                position: absolute;
                background-color: white;
                top: 12%;
                height: 75%;
                margin-left: 2.5%;
                border-radius: 20px;
                border: 2px solid black;
                text-align: center;
                width: 45%;
                font-size: 35px;
                font-weight: bold;
            }
            .datos{
                position: absolute;
                border: 2px solid black;
                text-align: center;
                border-radius: 20px;
                top: 12%;
                height: 75%;
                background-color: white;
                width: 45%;
                margin-left: 50%;
            }
            .datos_izq{
                position: relative;
                width: 50%;
                text-align: left;
            }
            .datos_der{
                position: absolute;
                top: 15.5%;
                margin-left: 50%;
                width: 45%;
                text-align: right;
            }
            .cajatexto{
	            padding: 12.5px;
	            margin: 10px;
	            border-radius: 7px;
	            border: 1px solid #660066;
	            width: 250px;
	            color: black;
	            background: white;
                font-family: 'Nunito', sans-serif;
            }
            #btn_registrar{
                font-family: 'Nunito', sans-serif;
                margin-top: 2.5%;
                font-size: 12px;
	            position: relative;
	            border-radius: 50px;
	            background-color: #660066;
	            color: white;
                font-size: 17px;
	            font-weight: bold;
	            position: relative;
	            border: 0px;
	            height: 60px;
	            width: 150px;
                border: 1px solid black;
            }
            #btn_registrar:hover{
                background-color: #9932CC;
	            border: 1px solid #660066;
	            cursor: pointer;
            }
            .div_tipo_usuario{
                margin-top: -75px;
                text-align: right;
                margin-right: 7.5%;
                margin-bottom: 5%;
            }
            .radiobutton{
                margin-left: 15px;
                font-size: 20px;
            }
            #btn_return{
                position: absolute;
                border-radius: 6px;
                top: -65px;
                margin-left: 95%;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                height: 30px;
	            width: 30px;
            }
            #btn_return:hover{
                background-color: #9932CC;
	            border: 1px solid #660066;
	            cursor: pointer;
            }
        </style>
    </head>
    <body>
    @include('sweetalert::alert')
        <div class="content">
            <div class="titulo">
                <br><h1>SIRHENA</h1>
                <img src="{{ asset('img/logo.png') }}" id="img_logo">
            </div>
            <div class="datos">
                <h1>Registrate Aqui</h1>
                <form action="" method="POST">
                    <div class="datos_izq">
                    @csrf
                        <input type="text" name="txt_cedula" id="txt_cedula" placeholder="Cedula/Cedula Juridica" title="Cedula" class="cajatexto" autocomplete="off"  maxlength="15" required/><br />
                        <input type="text" name="txt_username" id="txt_username" placeholder="Nombre de Usuario" title="Usuario" class="cajatexto" autocomplete="off"  maxlength="64" required/><br />
                        <input type="password" name="txt_password" id="txt_password" placeholder="**********" title="Contraseña" class="cajatexto" autocomplete="off"  maxlength="64" required/><br />
                        <input type="text" name="txt_nombre" id="txt_nombre" placeholder="Nombre completo/Nombre de la empresa" title="Nombre" class="cajatexto" autocomplete="off"  maxlength="64" required/><br />
                    </div>
                    <div class="datos_der">
                    @csrf
                        <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
                        <input type="text" name="txt_direccion" id="txt_direccion" placeholder="Direccion o Ubicacion" title="Direccion" class="cajatexto" autocomplete="off"  maxlength="264" required/><br />
                        <input type="text" name="txt_telefono" id="txt_telefono" placeholder="Numero de telefono" title="Telefono" class="cajatexto" autocomplete="off"  maxlength="32" required/><br />
                        <input type="email" name="txt_correo" id="txt_correo" placeholder="Correo Electronico" title="Email" class="cajatexto" autocomplete="off"  maxlength="64" required /><br />
                    </div>
                    <div class="div_tipo_usuario">
                        <h1>Tipo de Usuario</h1>
                        <label class="radiobutton">
                        @csrf
                            <input type="radio" name="rb_tipo_usuario" id="rb_candidato" class="tipo_usuario" value="C" required>Candidato
                        </label>
                        <label class="radiobutton">
                        @csrf
                            <input type="radio" name="rb_tipo_usuario" id="rb_empresa" class="tipo_usuario" value="E" required>Empresa
                        </label>
                    </div>
                    <input type="submit" value="REGISTRAR" name="btn_registrar" id="btn_registrar" /><br/>
                </form>
            </div>
        </div>
    </body>
</html>