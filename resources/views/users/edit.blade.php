<?php if ($_SESSION['datos_usuario']['logged_in'] == TRUE) { ?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SIRHENA-EDITAR PERFIL</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            html,
            body {
                background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
                background-color: silver;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow: auto;
            }

            .contenido {
                background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
                text-align: center;
            }

            .div_edicion {
                position: relative;
                background-color: white;
                margin-top: 2.5%;
                margin-left: 15%;
                border: 2px solid black;
                border-radius: 15px;
                width: 75%;
            }

            #div_nombre_usuario {
                position: relative;
                margin-left: 1.5%;
                text-align: left;
                font-size: 10px;
            }

            .div_foto {
                position: relative;
                color: transparent;
                margin-bottom: 2.5%;
            }

            .div_foto img {
                position: relative;
            }

            .foto_usuario {
                position: relative;
                clear: both;
                border-radius: 100px;
                border: 3px solid white;
            }

            .cajatexto {
                padding: 12.5px;
                margin: 10px;
                border-radius: 7px;
                border: 1px solid #660066;
                width: 250px;
                color: black;
                background: white;
                font-family: 'Nunito', sans-serif;
            }

            .datos_izq {
                position: relative;
                width: 45%;
                text-align: center;
            }

            .datos_der {
                position: relative;
                top: -322.5px;
                margin-left: 50%;
                width: 45%;
                text-align: center;
            }

            .btn_actions {
                font-family: 'Nunito', sans-serif;
                margin-top: 2.5%;
                font-size: 12px;
                position: relative;
                border-radius: 45px;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                font-size: 17px;
                font-weight: bold;
                position: relative;
                border: 0px;
                height: 30px;
                width: 35%;
                text-decoration: none;
            }

            .btn_actions:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            #div_btn_guardar {
                position: absolute;
                top: 79.2%;
                width: 55%;
            }

            #div_btn_eliminar {
                position: absolute;
                margin-left: 50%;
                top: 79%;
                width: 45%;
            }

            #btn_eliminar {
                position: absolute;
                font-family: 'Nunito', sans-serif;
                font-size: 12px;
                border-radius: 45px;
                background-color: #cc0000;
                color: white;
                font-size: 17px;
                font-weight: bold;
                position: relative;
                border: 0px;
                height: 60px;
                width: 55%;
                text-decoration: none;
            }

            #btn_eliminar:hover {
                background-color: red;
                border: 1.5px solid black;
                cursor: pointer;
            }

            #file_img {
                margin-left: 18%;
                border-radius: 5px;
            }

            #btn_cargar_img {
                font-family: 'Nunito', sans-serif;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                font-size: 12px;
                font-weight: bold;
                border-radius: 7px;
                width: 15%;
            }

            #btn_cargar_img:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            #btn_return {
                position: absolute;
                border-radius: 6px;
                top: -10px;
                margin-left: 30%;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                height: 30px;
                width: 30px;
            }

            #btn_return:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            #lbl_editar_perfil {
                margin-left: 33%;
                color: #636b6f;
                font-size: 55px;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
            }
        </style>
    </head>

    <body>
        <div class="contenido">
            <div class="div_edicion">
                <div id="div_nombre_usuario">
                    <h1><?php echo $_SESSION['datos_usuario']['nombre_real'] ?> </h1>
                    <label id="lbl_editar_perfil">Edita tu perfil!</label>
                    <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
                </div>
                <div class="div_foto">
                    @csrf
                    <img id="foto_usuario" src="{{ asset('img/logo.png') }}" alt="" width=100 height=100><br>
                    <!--<img src='" . site_url('/resources/photos/' . $this->session->userdata['logged_in']['photo']) 
                      . "' alt='Editar Foto' title='Editar Foto'  width=70 height=70 id='photo_profile' />"; ?>-->
                    <input type="file" name="txt_file" size="20" id="file_img" accept="image/jpeg,image/gif,image/png" /><br>
                    <button type="submit" id="btn_cargar_img">Cargar Foto</button>
                </div>
                <div class="datos_izq">
                    @csrf
                    <label for="txt_cedula">Cedula/Cedula Juridica</label><br>
                    <input type="text" name="txt_cedula" id="txt_cedula" value="<?php echo $_SESSION['datos_usuario']['cedula'] ?>" title="Cedula" class="cajatexto" autocomplete="off" maxlength="15" required /><br />
                    <label for="txt_username">Nombre de usuario</label><br>
                    <input type="text" name="txt_username" id="txt_username" value="<?php echo $_SESSION['datos_usuario']['username'] ?>" title="Usuario" class="cajatexto" autocomplete="off" maxlength="64" required /><br />
                    <label for="txt_password">Contraseña</label><br>
                    <input type="password" name="txt_password" id="txt_password" placeholder="**********" title="Contraseña" class="cajatexto" autocomplete="off" maxlength="64" required /><br />
                    <label for="txt_nombre" class="lbls">Nombre Completo</label><br>
                    <input type="text" name="txt_nombre" id="txt_nombre" value="<?php echo $_SESSION['datos_usuario']['nombre_real'] ?>" title="Nombre" class="cajatexto" autocomplete="off" maxlength="64" required /><br />
                </div>
                <div class="datos_der">
                    @csrf
                    <label for="txt_direccion" class="lbls">Direccion</label><br>
                    <input type="text" name="txt_direccion" id="txt_direccion" value="<?php echo $_SESSION['datos_usuario']['direccion'] ?>" title="Direccion" class="cajatexto" autocomplete="off" maxlength="264" required /><br />
                    <label for="txt_telefono" class="lbls">Telefono</label><br>
                    <input type="text" name="txt_telefono" id="txt_telefono" value="<?php echo $_SESSION['datos_usuario']['telefono'] ?>" title="Telefono" class="cajatexto" autocomplete="off" maxlength="32" required /><br />
                    <label for="txt_correo" class="lbls">Correo Electronico</label><br>
                    <input type="email" name="txt_correo" id="txt_correo" value="<?php echo $_SESSION['datos_usuario']['email'] ?>" title="Email" class="cajatexto" autocomplete="off" maxlength="64" required /><br />
                </div>
                <div class="actions">
                    <!--Div de acciones; guardar cambios, o eliminar usuario/cuenta, o donde se tiene que abrir el form-->
                    <div id="div_btn_guardar">
                        <a href="editarUsuario" name="btn_guardar" id="btn_guardar" title="Guardar Cambios" class="btn_actions">GUARDAR CAMBIOS</a>
                    </div>
                    <div id="div_btn_eliminar">
                        <a name="btn_eliminar" id="btn_eliminar" title="Eliminar Cuenta" href="eliminarUsuario">🗙 ELIMINAR MI CUENTA 🗙</a><br />
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>
<?php } ?>