<?php if ($_SESSION['datos_usuario']['logged_in'] == TRUE) { ?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SIRHENA-CURRICULUM</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            html,
            body {
                background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
                /*color: #636b6f;*/
                color: black;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow: auto;
            }

            .pagina {
                margin: auto;
                text-align: center;
                width: 100%;
                height: 100%;
            }

            .contenido {
                position: relative;
                margin-left: 2.5%;
                margin-bottom: 2.5%;
                width: 95%;
                margin-top: 2.5%;
                background-color: white;
                border-radius: 12px;
                border: 3px solid black;
            }

            .titulo {
                position: relative;
                margin-bottom: 5%;
            }

            #btn_return {
                position: absolute;
                border-radius: 6px;
                top: -12.5px;
                margin-left: 46.5%;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                height: 30px;
                width: 30px;
            }

            #btn_return:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .div_exp_prof {
                /*Div (lado izq) donde se van a agregar las experiencias profesionales, tipo como con los tweets del lab 6*/
                position: relative;
                width: 75%;
                margin-left: 12.5%;
                background-color: white;
                margin-bottom: 0.5%;
            }

            .btn_actions {
                position: relative;
                font-family: 'Nunito', sans-serif;
                font-size: 15px;
                text-decoration: none;
                background: unset;
                border-radius: 25px;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                font-weight: bold;
                border: 0px;
                height: 50px;
                width: 15%;
            }

            .btn_actions:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .btn_agregar {
                /*Div que corresponde a ambos botones de agregar*/
                position: relative;
                text-align: left;
                left: 1%;
                margin-bottom: 1%;
            }

            .div_form_academ {
                /*Div (lado der) donde se van a agregar las formaciones academicas, tipo como con los tweets del lab 6*/
                position: relative;
                width: 75%;
                margin-left: 12.5%;
                background-color: white;
            }

            .contenedor_info {
                /*Div contenedor dentro del cual se va a cargar la informacion, ya sean experiencias profesionales, o formaciones academicas*/
                position: relative;
                margin: auto;
                border-radius: 12px;
                border: 1.5px solid black;
                margin-bottom: 1.5%;
                overflow: auto;
            }

            .div_bottom {
                /*Div de abajo donde va el txt para observaciones y el boton para guardar todo*/
                position: relative;
                text-align: left;
            }

            #txt_observaciones {
                position: relative;
                margin-top: 1%;
                margin-left: 12.5%;
                margin-bottom: 1.5%;
                border: 1px solid #AAA;
                border-radius: 4px;
                width: 47.5%;
                height: 100px;
                background: white;
                color: black;
                text-align: justify;
            }

            #btn_guardar {
                position: absolute;
                top: 70px;
                margin-left: 8.5%;
            }

            .datos_curriculum {
                position: relative;
                margin-left: 17.5%;
                margin-bottom: 1%;
                margin-top: 1%;
                border: 1px solid black;
                border-radius: 12px;
                text-align: left;
                width: 65%;
                font-size: 15px;
                color: #33134C;
                border: 1px solid #33134C;
                border-radius: 7px;
                background: rgb(71, 48, 120, 0.2);
            }

            .actions_datos_curriculum {
                position: relative;
                text-align: right;
                margin-right: 1%;
                margin-bottom: 0.5%;
            }

            .btn {
                background: #FFF;
                color: #33134C;
                display: inline-flex;
                align-items: center;
                height: 26px;
                line-height: 26px;
                padding: 0 14px;
                font-size: 14px;
                border-radius: 3px;
                border: 1px solid #33134C;
            }

            .btn:hover {
                background: rgb(51, 19, 76, 0.2);
                cursor: pointer;
            }

            #btn_eliminar_experiencia {
                border: 1px solid #cc0000;
                color: #cc0000;
            }

            #btn_eliminar_experiencia:hover {
                background-color: red;
                border: 1px solid black;
                color: black;
                cursor: pointer;
            }

            #btn_eliminar_formacion {
                border: 1px solid #cc0000;
                color: #cc0000;
            }

            #btn_eliminar_formacion:hover {
                background-color: red;
                border: 1px solid black;
                color: black;
                cursor: pointer;
            }
        </style>
    </head>

    <body>
        @include('sweetalert::alert')
        <div class="pagina">
            <!--Div de la pagina completa, es decir el div container de todo lo demas-->
            <div class="contenido">
                <div class="titulo">
                    <!--Div donde va a ir la foto/nombre del usuario logeado-->
                    <h1>TU CURRICULUM</h1>
                    <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
                </div>
                <div class="div_exp_prof">
                    <!--Div (lado izq) donde se van a agregar las experiencias profesionales, tipo como con los tweets del lab 6-->
                    <h3>Experiencias Profesionales</h3>
                    <div class="contenedor_info">
                        <!--Dentro de este div se van a cargar las exp profs como los tweets-->
                        <!--Aqui tiene que venir el foreach para cargar las experiencias-->
                        <!--Div de las experiencias como tal-->
                        @foreach($experiencias as $e)
                        <div class="datos_curriculum" id={{$e->id_experiencias_profesionales}}>
                            <p>Puesto: {{$e->puesto}}</p>
                            <p>Empresa: {{$e->empresa}}</p>
                            <p>Descripcion: {{$e->descrip_responsabilidades}}</p>
                            <p>De: {{$e->fecha_inicio}} a {{$e->fecha_fin}}</p>
                            <div class="actions_datos_curriculum">
                                <a class="btn" id="btn_editar_experiencia">Editar</a>
                                <a class="btn" id="btn_eliminar_experiencia" href='eliminarExperiencias/{{$e->id_experiencias_profesionales}}'>Eliminar</a>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <div class="btn_agregar">
                        <!--Div donde va el boton para agregar-->
                        <a href="experiencias" name="btn_add_exp" id="btn_add_exp" title="Agregar Experiencia Profesional" class="btn_actions">AGREGAR</a><br />
                    </div>

                </div>
                <div class="div_form_academ">
                    <!--Misma vara que el div anterior pero va al lado derecho de la pantalla-->
                    <h3>Formaciones Academicas</h3>
                    <div class="contenedor_info">
                        <!--Aqui tiene que venir el foreach para cargar las formaciones academicas-->

                        <!--Div de la formaciones academica como tal-->
                        @foreach($formaciones as $f)
                        <div class="datos_curriculum" id={{$f->id_formaciones_academicas}}>
                            <p>Titulo: {{$f->titulo}}</p>
                            <p>Especialidad: {{$f->especialidad}}</p>
                            <p>Institucion: {{$f->institucion}}</p>
                            <p>Fecha: {{$f->fecha}} </p>
                            <div class="actions_datos_curriculum">
                                <a class="btn" id="btn_editar_formacion">Editar</a>
                                <a class="btn" id="btn_eliminar_formacion" href='eliminarFormacion/{{$f->id_formaciones_academicas}}'>Eliminar</a>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <div class="btn_agregar">
                        <a href="formaciones" name="btn_add_formacadem" id="btn_add_formacadem" title="Agregar Formacion Academica" class="btn_actions">AGREGAR</a><br />
                    </div>
                </div>
                <form method="post">
                    @csrf
                    <div class="div_bottom">
                        <!--Div de la parte de abajo donde va ir el txt para agregar meritos u observaciones, y el boton de guardar-->
                        <textarea cols="4" rows="6" id="txt_observaciones" name="txt_observaciones" placeholder="Agrega méritos u observaciones que desees hacer constar!" maxlength="256">@if($curriculum->meritos_observaciones!=null||$curriculum->meritos_observaciones!=''){{$curriculum->meritos_observaciones}}@endif</textarea>
                        <input type="submit" value="GUARDAR" name="btn_guardar" id="btn_guardar" title="Guardar cambios realizados" class="btn_actions" /><br />
                    </div>
                </form>
            </div>
        </div>
    </body>

    </html>
<?php } ?>
