<!DOCTYPE html>
<html>

<head>
    <meta lang='en'>
    <style>
        html,
        body {
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
        }

        #banner_box {
            position: absolute;
            margin: auto auto;
            text-align: center;
            width: 100%;
            height: 108px;
            top: 0px;
            font-size: 20px;
            margin-bottom: 5%;
        }

        #banner {
            position: relative;
            object-fit: cover;
            object-position: center;
            height: 108px;
            width: 360px;

        }

        #btn_regresar {
            position: absolute;
            right: 5px;
        }

        #barra {
            top: 100px;
            position: relative;
            background: #382B73;
            width: 100%;
            height: 5px;
        }

        .contenido_box {
            position: relative;
            width: 75%;
            margin: auto auto;
            top: 100px;
            border: 2px solid black;
            border-radius: 20px;
        }

        .block {
            position: relative;
            width: 75%;
            margin-top: 1%;
            margin-left: 10%;
            margin-bottom: 2.5%;
            padding: 5px 15px 15px;
            border: 1px solid #33134C;
            border-radius: 7px;
            background: rgb(71, 48, 120, 0.2);
        }


        .contenido_content {
            font-size: 15px;

            color: #33134C;
        }

        .img_perfil {
            height: 100px;
            width: 80px;


        }

        .btn {
            background: #FFF;
            color: #33134C;
            display: inline-flex;
            align-items: center;
            height: 26px;
            line-height: 26px;
            padding: 0 14px;
            font-size: 14px;
            border-radius: 3px;
            border: 1px solid #33134C;
        }

        .btn:hover {
            background: rgb(51, 19, 76, 0.2);
            cursor: pointer;

        }

        label {
            margin-left: 20px;
        }

        h3 {
            object-position: center;
            color: #33134C;
        }

        #btn_return {
            position: absolute;
            border-radius: 6px;
            top: 10px;
            margin-left: 95%;
            background-color: #660066;
            border: 1.5px solid black;
            color: white;
            height: 30px;
            width: 30px;
        }

        #btn_return:hover {
            background-color: #9932CC;
            border: 1px solid #660066;
            cursor: pointer;
        }

        #lbl_candidatos {
            position: relative;
            margin-left: 10%;
            color: #636b6f;
            font-size: 55px;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
        }
    </style>
</head>

<body>

    <div id='banner_box'>
        <img id="banner" src={{asset('img/SIRHENA_LOGO.png')}}>
    </div>
    <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
    <div id='barra'></div><br><br>
    <div class="contenido_box">
    <label id="lbl_candidatos">Candidatos aspirantes a tu oferta!</label>
        @csrf
        @foreach($candidato as $c)
        <div class="block">
            <div class="contenido_content">
                <!--<img class="img_perfil" src={{asset('img/SIRHENA_LOGO.png')}}>-->
                <label id="cont_serv">Nombre: {{$c->nombre_real}}</label>
                <label id="cont_serv">Cedula: {{$c->cedula}}</label><br>
                <label id="cont_serv">Telefono: {{$c->telefono}}</label><br>
                <label id="cont_serv">Correo electronico: {{$c->email}}</label><br>
            </div>
        </div>
        @endforeach
    </div>
</body>

</html>