<?php if ($_SESSION['datos_usuario']['logged_in'] == TRUE) { ?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SIRHENA-OFERTA LABORAL</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            html,
            body {
                background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                overflow: auto;
            }

            .pagina {
                margin: auto;
                text-align: center;
                width: 100%;
                height: 100%;
            }

            .contenido {
                position: relative;
                margin-left: 2.5%;
                margin-bottom: 2.5%;
                width: 95%;
                margin-top: 1%;
                background-color: white;
                border-radius: 12px;
                border: 3px solid black;
            }

            .titulo {
                position: relative;
                margin: auto;
                text-align: center;
                margin-bottom: 2.5%;
            }

            #lbl_nom_empresa {
                position: relative;
                /*left: -42.5%;*/
            }

            .datos {
                /*Div donde van todos los datos que se solicitan*/
                background-color: transparent;
            }

            .vacafe {
                margin-left: 1.5%;
                margin-right: 15%;
            }

            #btn_return {
                position: absolute;
                border-radius: 6px;
                top: 5px;
                margin-left: 46.5%;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                height: 30px;
                width: 30px;
            }

            #btn_return:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            .cajatexto {
                padding: 12.5px;
                margin: 10px;
                border-radius: 7px;
                border: 1px solid #660066;
                width: 250px;
                color: black;
                background: white;
                font-family: 'Nunito', sans-serif;
            }

            .btn_actions {
                font-family: 'Nunito', sans-serif;
                font-size: 12.5px;
                position: relative;
                border-radius: 25px;
                background-color: #660066;
                border: 1.5px solid black;
                color: white;
                font-weight: bold;
                position: relative;
                border: 0px;
                height: 30px;
                width: 15%;
                margin-bottom: 1.5%;
            }

            .btn_actions:hover {
                background-color: #9932CC;
                border: 1px solid #660066;
                cursor: pointer;
            }

            #btn_guardar {
                position: relative;
                margin-bottom: 1.5%;
            }

            .contenedor_requisitos {
                position: relative;
                text-align: center;
                width: 80%;
                margin: auto;
                border-radius: 12px;
                border: 1.5px solid black;
                margin-bottom: 1.5%;
            }

            .btn_agregar {
                /*Div que corresponde al boton de agregar*/
                position: relative;
                text-align: right;
                right: 10%;
                top: -35px;
                margin-bottom: -2.5%;
            }

            #btn_agregar_requisito {
                position: relative;
                width: 10%;
            }

            #txt_requisito {
                position: relative;
                left: -5%;
                width: 65%;
                height: 27.5px;
            }

            #txt_ubicacion {
                position: relative;
                width: 26%;
            }

            #txt_descripcion {
                position: relative;
                width: 48%;
            }

            .requisitos_block {
                position: relative;
                overflow: auto;
            }

            /*Codigo css que tiene que ver directamente con los requisitos que se agregan al div, como con los tweets*/
            .detalle_requisito {
                position: relative;
                border-radius: 7px;
                border: 0.5px solid black;
                border-top: 2px solid #660066;
                background-color: silver;
                text-align: left;
                margin: 0.5%;
            }

            .info_requi {
                /*class de los lbl que hacen referencia a la informacion del requisito*/
                margin-left: 0.5%;
            }

            .requisitos_actions {
                position: absolute;
            }

            .actions_requisito {
                /*Class de los href que funcionan de botones de editar y eliminar requisito*/
                margin-right: 0.5%;
                font-size: 20px;
                text-decoration: none;
                background: unset;
            }

            #btn_edit_req {
                color: green;
                margin-left: 92.5%;
            }

            #btn_elim_req {
                color: red;
            }
        </style>
    </head>

    <body>
        <script>
            function editar_requisito() { //Metodo para poner dentro del txt de requisito, el requisito que se desea editar
                var txt_requisito = document.getElementById('txt_requisito');
                txt_requisito.innerHTML = "PENE";
            }
        </script>

        <div class="pagina">
            <!--Div de la pagina completa-->
            <div class="contenido">
                <!--Dentro de este div va a ir todo el contenido-->
                <div class="titulo">
                    <!--Div dentro del cual va el titulo y la foto/nombre de la empresa-->
                    <label id="lbl_nom_empresa"><?php echo $_SESSION['datos_usuario']['nombre_real'] ?></label>
                    <h1 id="h1_titulo">Oferta de Empleo</h1>
                    <!--<button type="submit" name="btn_return" id="btn_return" class="boton" title="Regresar">←</button>-->
                    <a href="{{ URL::previous() }}" name="btn_return" id="btn_return" class="boton" title="Regresar">←</a>
                </div>
                <div class="datos">
                    <!--Div donde van todos los datos que se solicitan-->
                    <div class="">
                        <!--Div donde se piden la cantidad de VAcantes, CAtegoria, y la FEcha de la oferta (VACAFE)-->
                        <input id="cant_vacantes" name="cant_vacantes" type="number" required placeholder="Cantidad de vacantes" class="cajatexto" min="1" max="11" required>
                        <input type="date" name="txt_fecha" id="txt_fecha" min="" class="cajatexto" required>
                        @csrf
                        <select name="cmb_categoria" id="cmb_categoria" class="cajatexto">
                            <!--Combo box con las categorias, en min tenemos que poner la fecha actual-->
                            @foreach($categorias as $c)
                            <option value={{$c->id_categoria_laboral}}>{{$c->categoria}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="div_requisitos">
                        <h3>Requisitos</h3>
                        <!--Abrir form para metodo de agregar un requisito-->
                        <form action="" method="POST">
                            @csrf
                            <textarea cols="" rows="" id="txt_requisito" name="txt_requisito" placeholder="Escribe un requisito para esta oferta laboral!" class="cajatexto" maxlength="128"></textarea>
                            <div class="btn_agregar">
                                <!--Div donde va el boton para agregar-->
                                <input type="submit" value="AGREGAR" name="btn_agregar_requisito" id="btn_agregar_requisito" title="Agregar Requisito" class="btn_actions" /><br />
                            </div>
                        </form>
                        <!--Cierra form para agregar requisito-->
                        <div class="contenedor_requisitos">
                            <div class="requisitos_block">
                                <!--Div, codigo html del requisito (como un tweet del lab6) como tal-->
                                <!--Aqui tiene que venir el foreach para cargar las experiencias-->
                                @foreach ($requisitos as $r)
                                <div class="detalle_requisito">
                                    <!--Div donde va a ir el texto/detalle del requisito-->
                                    <label for="requisito" class="info_requi">Requisito:</label>
                                    <label for="" class="info_requi" id="requisito">{{$r->requisito}}</label>
                                    <input type="hidden" value="{{$r->id_requisito_laboral}}">
                                    <div class="requisito_actions">
                                        <a href="" id="btn_edit_req" title="Editar Requisito" class="actions_requisito" onclick="editar_requisito()">✎</a> <!-- {{$r->id_requisito_laboral}}-->
                                        <a href="elim_requisito" id="btn_elim_req" title="Eliminar Requisito" class="actions_requisito">🗙</a>
                                        <!--{{$r->id_requisito_laboral}}-->
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                    <div class="div_textos">
                        <!--Div donde van a ir los txt por asi decirlo, los campos de direccion/ubicacion, y la descripcion del puesto vacante-->
                        <textarea type="text" name="txt_ubicacion" id="txt_ubicacion" autocomplete="off" class="cajatexto" title="" placeholder="Lugar de trabajo (direccion) de la oferta" maxlength="512" required></textarea>
                        <textarea cols="" rows="" id="txt_descripcion" name="txt_descripcion" placeholder="Descripcion del puesto(s) vacante" class="cajatexto" maxlength="512" required></textarea>
                    </div>
                    <div class="div_bottom">
                        <!--Div de la parte de abajo de la pantalla, donde va el boton de guardar-->
                        <input type="submit" value="SUBIR OFERTA" name="btn_guardar" id="btn_guardar" title="Guardar cambios realizados" class="btn_actions" /><br />
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>
<?php } ?>