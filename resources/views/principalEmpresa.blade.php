<?php if($_SESSION['datos_usuario']['logged_in'] == TRUE) { ?>

<!DOCTYPE html>
<html>

<head>
  <meta lang='en'>
  <style>

    html, body {
      font-family: 'Nunito', sans-serif;
      font-weight: 200;
    }
    .menu_btn {
      background-color: #473080;
      color: white;
      padding: 16px;
      font-size: 40px;
      border: none;
      border-radius: 30px;
    }

    .menu_box {
      position: relative;
      border-radius: 15px;
      display: inline-block;
    }

    .menu-content {
      display: none;
      position: absolute;
      background: rgb(255, 255, 255, 0.9);
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
      z-index: 1;
    }

    .menu-content a {
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
    }

    .menu-content a:hover {
      background: rgb(51, 19, 76, 0.2);
    }

    .menu_box:hover .menu-content {
      display: block;
    }

    #banner_box {
      position: absolute;
      margin: auto auto;
      text-align: center;
      width: 100%;
      height: 108px;
      top: 0px;
      font-size: 20px;

    }

    #banner {
      position: relative;
      object-fit: cover;
      object-position: center;
      height: 108px;
      width: 360px;

    }

    #barra {
      top: 20px;
      position: relative;
      background: #382B73;
      width: 100%;
      height: 5px;
    }

    #busqueda_box {
      border-radius: 12px;
      border: 3px solid black;
      background: #382B73;
      position: relative;
      top: 25px;
      display: block;
      margin-left: auto;
      margin-right: auto;
      height: 12%;
      margin-bottom: 2.5%;
    }

    .busqueda-content {
      position: relative;
      width: 100px;
      color: #FFF;
      font-size: 20px;
      color: black;
    }

    #cmb_categoria {
      background: #CCC;
      font-size: 15px;
      border-radius: 25px;
      width: 300px;
      color: #000;
    }

    #cmb_aplicado {
      background: #CCC;
      font-size: 15px;
      border-radius: 25px;
      width: 300px;
      color: #000;
    }

    #txt_nombre {
      background: #CCC;
      border-radius: 25px;
      border-style: none;
      width: 400px;
      color: #000;
    }
    #btn_buscar {
      font-family: 'Nunito', sans-serif;
      font-size: 17px;
      position: relative;
      border-radius: 50px;
      background-color: silver;
      color: black;
      position: relative;
      border: 2px solid black;
      height: 50px;
      width: 100px;
    }
    #btn_buscar:hover {
      background-color: grey;
      cursor: pointer;
    }
    .contenido_box {
      position: relative;
      border-radius: 12px;
      border: 2px solid black;
      width: 75%;
      margin: auto auto;
    }
    .block {
      position: relative;
      width: 60%;
      margin-top: 1%;
      margin-left: 17.5%;
      margin-bottom: 2.5%;
      padding: 5px 15px 15px;
      border: 1px solid #33134C;
      border-radius: 7px;
      background: rgb(71, 48, 120, 0.2);
    }
    .contenido_content {
      font-size: 15px;
      color: #33134C;
    }
    .btn {
      background: #FFF;
      color: #33134C;
      display: inline-flex;
      align-items: center;
      height: 26px;
      line-height: 26px;
      padding: 0 14px;
      font-size: 14px;
      border-radius: 3px;
      border: 1px solid #33134C;
    }
    .btn:hover {
      background: rgb(51, 19, 76, 0.2);
      cursor: pointer;
    }
    #btn_eliminar_oferta{
      border: 1px solid #cc0000;
      color: #cc0000;
    }
    #btn_eliminar_oferta:hover {
      background-color: red;
      border: 1px solid black;
      color: black;
      cursor: pointer;
    }
    .actions_oferta{
      text-align: right;
    }
    .elementos_busqueda_box{
      margin-top: 1%;
      margin-left: 25%;
      text-align: left;
      width: 45%;
    }
    .btn_busqueda{
      position: relative;
      align-items: left;
      top: -55px;
      margin-top: 1%;
      margin-left: 60%;
      width: 15%;
    }
    #lbl_busqueda{
      display: block;
      color: #FFF;
      font-size: 20px;
      margin-bottom: 1.5%;
    }
    #lbl_ofertas_laborales{
      margin-left: 33%;
      color: #636b6f;
      font-size: 55px;
      font-family: 'Nunito', sans-serif;
      font-weight: 200;
    }
  </style>
</head>

<body>
@include('sweetalert::alert')
  <!-- <php session_start()?>--><!--Para poder dar uso a la variable global $_SESSION-->
  <div id='banner_box'>
    <img id="banner" src={{asset('img/SIRHENA_LOGO.png')}}>
  </div>
  <div class="menu_box">
    <button class="menu_btn">🏭</button>
    <div class="menu-content">
      <a href="add_oferta">Agregar Oferta Laboral</a>
      <a href="edit_perfil">Editar Perfil</a>
      <a href="logout">Cerrar Sesión</a>
    </div>
  </div>
  <div id='barra'></div>

  <div id="busqueda_box">
    <div class="elementos_busqueda_box">
      <label id="lbl_busqueda" for="cmb_categoria">Filtra tus ofertas laborales por categoria!</label>
      @csrf
      <select name="cmb_categoria" id="cmb_categoria" >
      @foreach($categorias as $c)
          <option value={{$c->id_categoria_laboral}}>{{$c->categoria}}</option>
      @endforeach
      </select>
    </div>
    <div class="btn_busqueda">
      <button class="busqueda-content" id="btn_buscar">Buscar</button>
    </div>
  </div>
  <label id="lbl_ofertas_laborales">Ofertas Laborales</label>
  @csrf
  <div class="contenido_box">
  @foreach($ofertas as $o)
    <div class="block">
      <div class="contenido_content">
        <p id="cont_serv">Categoria:{{$o->categoria}}</p>
        <p id="cont_serv">Descripcion:{{$o->descrip_puesto}}</p>
        <p id="cont_serv">Vacantes:{{$o->cantidad_vacantes}}</p>
        <p id="cont_serv">Fecha:{{$o->fecha_oferta}}</p>
        <p id="cont_serv">Ubicacion:{{$o->ubicacion}}</p>
        <div class="actions_oferta">
          <a class="btn" id="btn_ver_candidatos" href = 'candidatos/{{$o->id_oferta_laboral}}'>Ver candidatos</a>
          <a class="btn" id="btn_editar_oferta">Editar</a>
          <a class="btn" href = 'eliminarOferta/{{$o->id_oferta_laboral}}' id="btn_eliminar_oferta">Eliminar</a>
        </div>
      </div>
    </div>
  @endforeach
    
  </div>

</body>

</html>
<?php } ?>