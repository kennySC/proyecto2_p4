<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SIRHENA</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background: radial-gradient(circle, rgba(166, 14, 146, 0.9402135854341737) 1%, rgba(174, 37, 230, 0.9009978991596639) 69%, rgba(132, 20, 214, 0.9150035014005602) 96%);
            background-color: white;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
            overflow: hidden;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            /*align-items: center;*/
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
            width: 100%;
        }

        .title {
            font-size: 85px;
        }

        #div_titulo {
            text-align: center;
            margin-left: 12.5%;
            margin-top: 2.5%;
            width: 75%;
            border-radius: 15px;
            background-color: white;
            box-shadow: 0px 5px #660066;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            color: #660066;
            font-weight: bold;
            text-shadow: 2px 3px grey;
        }

        .subtitulo {
            margin-bottom: 30px;
            font-size: 20px;
            color: black;
            font-style: italic;
            font-weight: bold;
        }

        .login {
            width: 75%;
            margin-left: 13%;
        }

        .datos {
            background-color: #660066;
            border: 2px solid black;
            border-radius: 12px;
        }

        .lbl {
            font-size: 20px;
            color: black;
            font-weight: bold;
        }

        .logo {
            border-radius: 20px;
            text-align: center;
        }

        .cajatexto {
            padding: 15px;
            margin: 8px;
            border-radius: 7px;
            border: 2px solid black;
            width: 350px;
            color: black;
            background: white;
            font-family: 'Nunito', sans-serif;
        }

        #btn_login {
            font-family: 'Nunito', sans-serif;
            position: relative;
            border-radius: 50px;
            background-color: gold;
            color: black;
            font-size: 17px;
            font-weight: bold;
            position: relative;
            border: 2px solid black;
            height: 60px;
            width: 120px;
        }

        #btn_login:hover {
            background-color: yellow;

            cursor: pointer;
        }

        #actions a {
            color: white;
            font-size: 16px;
            margin: 5px;
            cursor: pointer;
        }
        #lbl_integrantes{
            position: relative;
            font-weight: bold;
            color: black;
        }
    </style>
</head>

<body>
    @include('sweetalert::alert')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div id="div_titulo">
                <div class="logo">
                    <img src="{{ asset('img/logo.png') }}" id="img_logo">
                </div>
                <div class="title subtitulo">
                    -Sistema de Recursos Humanos y Empleo Nacional-
                </div>
            </div>
            <div class="login datos">
                <label class="lbl">LOGIN</label>
                <div class="div_datos">
                    <form method="POST">
                        @csrf
                        <input type="text" name="txt_username" id="txt_username" placeholder="USERNAME" title="Usuario" class="cajatexto" autocomplete="off" /><br />
                        <input type="password" name="txt_password" id="txt_password" placeholder="**********" title="Contraseña" class="cajatexto" /><br />
                        <input type="submit" value="INGRESAR" name="btn_login" id="btn_login" /><br />
                        <br>
                    </form>
                    <div id="actions">
                        <a href="registrarse" id="btn_adduser" name="btn_adduser" title="Registrarse">¿No tienes cuenta? Registrate</a>
                    </div>
                </div>
            </div><br><br><br><br>
            <label id="lbl_integrantes">Proyecto realizado por: Matthew Miranda Araya, Djorkaeff Peppard Romero y Kenneth Sibaja Castro</label>
        </div>
    </div>
</body>

</html>